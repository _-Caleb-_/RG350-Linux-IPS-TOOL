# Linux IPS TOOL

OPK Version: 1.0
Release date: 16/06/2022

Packed by: _-Caleb-_ 
Telegram: @calebin


Apply IPS patchs in your roms without external APPS using the console.

Lips website: [https://github.com/kylon/Lipx](https://github.com/kylon/Lipx)


## Keys:

- L1:     Switch between Folder/File Box
- X:      Select Current File
- Start:  Continue with the selected file

## ScreenShots

![](https://codeberg.org/_-Caleb-_/RG350-Linux-IPS-TOOL/raw/branch/main/img/1.jpg)
![](https://codeberg.org/_-Caleb-_/RG350-Linux-IPS-TOOL/raw/branch/main/img/2.jpg)
![](https://codeberg.org/_-Caleb-_/RG350-Linux-IPS-TOOL/raw/branch/main/img/3.jpg)
