#Parcheador de IPS a ROM para RG350
#!/bin/sh

DIR=$HOME/tmp
#Selecciona PARCHE
archivoips=$(dialog --title "Select IPS file" --stdout --fselect /media/sdcard/  14 46)
#Selecciona ROM
archivorom=$(dialog --title "Select ROM file" --stdout --fselect /media/sdcard/  14 46)

#Parchea
clear
python lipx.py -a "$archivorom" "$archivoips"
echo ""
echo "Linux IPS Patcher v 1.0..."
echo "...for RG350 Family by _-Caleb-_"
echo ""
sleep 4

